# libjavascript
This library helps C programs interact with Javascript types. This library provides wrappers for the following Javascript types;

- number
- string
- boolean
- Array
- Object

## Example
Below is an example function which builds a login request from two objects

```c
const char *build_login_request(const char *username, const char *password) {
  JSValue *data = js_object();
  object_put(data, "username", js_string(username));
  object_put(data, "password", js_string(password));

  JSValue *request = js_object();
  object_put(request, "action", js_string("login"));
  object_put(request, "data", data);

  return json_stringify(request);
}
```

This function would emit JSON like this;

```
{
  "action": "login",
  "data": {
    "username": "somebody@example.com",
    "password": "super_secure_password"
  }
}
```

## License
MIT License

Copyright (c) 2020 Steven Ian Milne

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
