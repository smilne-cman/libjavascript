#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include "types.h"
#include "array.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void array_push(JSValue *array, JSValue *item) {
  if (array->type != JSArray) return;

  List *items = (List *)array->value;
  list_add(items, item);
}

int array_length(JSValue *array) {
  if (array->type != JSArray) return 0;

  List *items = (List *)array->value;
  return list_size(items);
}

JSValue *array_get(JSValue *array, int index) {
  if (array->type != JSArray) return NULL;

  List *items = (List *)array->value;

  return list_get(items, index);
}
