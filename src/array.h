#ifndef __array__
#define __array__

/* Includes *******************************************************************/
#include "types.h"

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
void array_push(JSValue *array, JSValue *item);
int array_length(JSValue *array);
JSValue *array_get(JSValue *array, int index);

#endif
