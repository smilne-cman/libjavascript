#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libcollection/compare.h>
#include <libcollection/list.h>
#include <libcollection/map.h>
#include "types.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/
const char JS_UNDEFINED[] = "undefined";
const char JS_NULL[] = "null";
const char JS_STRING[] = "string";
const char JS_NUMBER[] = "number";
const char JS_BOOLEAN[] = "boolean";
const char JS_OBJECT[] = "Object";
const char JS_ARRAY[] = "Array";
const char JS_TRUE[] = "true";
const char JS_FALSE[] = "false";

/* Functions ******************************************************************/

JSValue *js_value(JSType type, void *value) {
  JSValue *wrapper = (JSValue *)malloc(sizeof(JSValue));

  wrapper->type = type;
  wrapper->value = value;

  return wrapper;
}

JSValue *js_undefined() {
  return js_value(JSUndefined, NULL);
}

JSValue *js_null() {
  return js_value(JSNull, NULL);
}

JSValue *js_string(const char *value) {
  return js_value(JSString, value);
}

JSValue *js_number(long value) {
  long *reference = (long *)malloc(sizeof(long));
  *reference = value;

  return js_value(JSNumber, value);
}

JSValue *js_boolean(int value) {
  int *reference = (int *)malloc(sizeof(int));
  *reference = value;

  return js_value(JSBoolean, value);
}

JSValue *js_object() {
  Map *properties = map_new(map_by_string(void_compare));

  return js_value(JSObject, properties);
}

JSValue *js_array() {
  List *items = list_new(ListAny);

  return js_value(JSArray, items);
}

const char *js_type_string(JSType type) {
  switch (type) {
    case JSUndefined:
      return JS_UNDEFINED;
    case JSNull:
      return JS_NULL;
    case JSString:
      return JS_STRING;
    case JSNumber:
      return JS_NUMBER;
    case JSBoolean:
      return JS_BOOLEAN;
    case JSObject:
      return JS_OBJECT;
    case JSArray:
      return JS_ARRAY;
    default:
      return "error";
  }
}
