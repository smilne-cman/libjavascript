#ifndef __types__
#define __types__

/* Includes *******************************************************************/

/* Types **********************************************************************/
typedef enum JSType {
  JSUndefined,
  JSNull,
  JSString,
  JSNumber,
  JSBoolean,
  JSObject,
  JSArray
} JSType;

typedef struct JSValue {
  JSType type;
  void *value;
} JSValue;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
extern const char JS_UNDEFINED[];
extern const char JS_NULL[];
extern const char JS_STRING[];
extern const char JS_NUMBER[];
extern const char JS_BOOLEAN[];
extern const char JS_OBJECT[];
extern const char JS_ARRAY[];
extern const char JS_TRUE[];
extern const char JS_FALSE[];

JSValue *js_value(JSType type, void *value);
JSValue *js_undefined();
JSValue *js_null();
JSValue *js_string(const char *value);
JSValue *js_number(long value);
JSValue *js_boolean(int value);
JSValue *js_object();
JSValue *js_array();

const char *js_type_string(JSType type);

#endif
