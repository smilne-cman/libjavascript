#ifndef __object__
#define __object__

/* Includes *******************************************************************/
#include "types.h"

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
void object_put(JSValue *object, char *key, JSValue *value);
JSValue *object_get(JSValue *object, char *key);
void object_delete(JSValue *object, char *key);

#endif
