#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libcollection/map.h>
#include "types.h"
#include "object.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void object_put(JSValue *object, char *key, JSValue *value) {
  if (object->type != JSObject) return;

  Map *properties = (Map *)object->value;
  map_put(properties, key, value);
}

JSValue *object_get(JSValue *object, char *key) {
  if (object->type != JSObject) return NULL;

  Map *properties = (Map *)object->value;
  JSValue *value = map_get(properties, key);

  return value != NULL ? value : js_undefined();
}

void object_delete(JSValue *object, char *key) {
  if (object->type != JSObject) return;

  Map *properties = (Map *)object->value;
  map_remove(properties, key);
}
