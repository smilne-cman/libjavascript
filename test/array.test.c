#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libtest/libtest.h>
#include "array.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_push();
void test_get();

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("array", argc, argv);

  test("Push", test_push);
  test("Get", test_get);

  return test_complete();
}

void test_push() {
  JSValue *array = js_array();

  test_assert_int(array_length(array), 0, "Should start with an empty array");

  array_push(array, js_null());
  array_push(array, js_number(123));
  array_push(array, js_boolean(TRUE));
  array_push(array, js_string("Hello world!"));

  test_assert_int(array_length(array), 4, "Should add all items to the array");
}

void test_get() {
  JSValue *array = js_array();

  array_push(array, js_null());
  array_push(array, js_number(123));
  array_push(array, js_boolean(TRUE));
  array_push(array, js_string("Hello world!"));

  JSValue *value1 = array_get(array, 0);
  JSValue *value2 = array_get(array, 1);
  JSValue *value3 = array_get(array, 2);
  JSValue *value4 = array_get(array, 3);

  test_assert(value1->value == NULL, "Should get null items");
  test_assert(value2->value == 123, "Should get numeric items");
  test_assert(value3->value == TRUE, "Should get boolean items");
  test_assert(!strcmp(value4->value, "Hello world!"), "Should get string items");
}
