#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libtest/libtest.h>
#include "object.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_get_put();

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("object", argc, argv);

  test("Get & Put", test_get_put);

  return test_complete();
}

void test_get_put() {
  JSValue *object = js_object();

  object_put(object, "value1", js_null());
  object_put(object, "value2", js_number(123));
  object_put(object, "value3", js_boolean(TRUE));
  object_put(object, "value4", js_string("Hello world!"));

  JSValue *value1 = object_get(object, "value1");
  JSValue *value2 = object_get(object, "value2");
  JSValue *value3 = object_get(object, "value3");
  JSValue *value4 = object_get(object, "value4");

  test_assert(value1->value == NULL, "Should put and get null properties");
  test_assert(value2->value == 123, "Should put and get numeric properties");
  test_assert(value3->value == TRUE, "Should put and get boolean properties");
  test_assert(!strcmp(value4->value, "Hello world!"), "Should put and get string properties");
}
