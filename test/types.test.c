#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include "types.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_undefined();
void test_null();
void test_string();
void test_number();
void test_boolean();
void test_object();
void test_array();

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("types", argc, argv);

  test("Undefined", test_undefined);
  test("Null", test_null);
  test("String", test_string);
  test("Number", test_number);
  test("Boolean", test_boolean);
  test("Object", test_object);
  test("Array", test_array);

  return test_complete();
}

void test_undefined() {
  JSValue *value = js_undefined();

  test_assert(value->type == JSUndefined, "Should have type of 'undefined'");
  test_assert(value->value == NULL, "Should have a value of NULL");
}

void test_null() {
  JSValue *value = js_null();

  test_assert(value->type == JSNull, "Should have type of 'null'");
  test_assert(value->value == NULL, "Should have a value of NULL");
}

void test_string() {
  char *string = "Hello world!";
  JSValue *value = js_string(string);

  test_assert(value->type == JSString, "Should have type of 'string'");
  test_assert(value->value == string, "Should have a value which points to the string");
}

void test_number() {
  long number = 12345;
  JSValue *value = js_number(&number);

  test_assert(value->type == JSNumber, "Should have type of 'number'");
  test_assert(value->value == &number, "Should have a value which points to the number");
}

void test_boolean() {
  int boolean = TRUE;
  JSValue *value = js_boolean(boolean);

  test_assert(value->type == JSBoolean, "Should have type of 'boolean'");
  test_assert(value->value == TRUE, "Should have a value which points to the boolean");
}

void test_object() {
  JSValue *value = js_object();

  test_assert(value->type == JSObject, "Should have type of 'Object'");
  test_assert(value->value != NULL, "Should have a value which points to the map of properties");
}

void test_array() {
  JSValue *value = js_array();

  test_assert(value->type == JSArray, "Should have type of 'Array'");
  test_assert(value->value != NULL, "Should have a value which points to the list of items");
}
